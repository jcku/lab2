/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wbn
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return "" + this.getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float input = this.getValue();
		input = ((input - 32) * 5/9);
		
		Temperature ret = new Celsius(input);
		return ret;
	
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float input = this.getValue();
		
		Temperature ret = new Fahrenheit(input);
		
		return ret;
	}
	
	public Temperature toKelvin() {
		float input = this.getValue();
		input = (float) (((input - 32.0)*5/9) + 273.15);
		
		Temperature ret = new Kelvin(input);
	
		return ret;
	}
}
