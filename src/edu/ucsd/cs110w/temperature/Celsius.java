/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wbn
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return "" + this.getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float input = this.getValue();
		
		Temperature ret = new Celsius(input);
		return ret;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float input = this.getValue();
		input = (((input*9)/5)+32);
		
		Temperature ret = new Fahrenheit(input);
		return ret;
	}
	
	public Temperature toKelvin() {
		float input = this.getValue();
		input = input + (float)273.15;
		Temperature ret = new Kelvin(input);
		
		return ret;
	}
	
	
}
